﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace CSIMediaTest.Models
{
    public class DatabaseAccess
    {
        private SqlConnection connection;

        private void Connection()
        {
            var conString = ConfigurationManager.ConnectionStrings["NumbersConn"].ToString();
            connection = new SqlConnection(conString);
        }

        // Add Numbers
        public bool AddNumbers(Numbers numModel)
        {
            Connection();

            var cmd = new SqlCommand("AddNumbers", connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.AddWithValue("@NumberEnties", numModel.NumberEnties);
            cmd.Parameters.AddWithValue("@IsAscending", numModel.IsAscending);
            cmd.Parameters.AddWithValue("@TimeTaken", numModel.TimeTaken);

            connection.Open();
            var successful = cmd.ExecuteNonQuery();
            connection.Close();

            if (successful >= 1)
                return true;
            else
                return false;
        }

        // Get Numbers
        public List<Numbers> GetNumbers()
        {
            Connection();

            var numbers = new List<Numbers>();
            var cmd = new SqlCommand("GetNumbers", connection)
            {
                CommandType = CommandType.StoredProcedure
            };

            var adapter = new SqlDataAdapter(cmd);
            var dt = new DataTable();

            connection.Open();
            adapter.Fill(dt);
            connection.Close();

            foreach (DataRow dr in dt.Rows)
            {
                numbers.Add(
                    new Numbers
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        NumberEnties = Convert.ToString(dr["NumberEnties"]),
                        IsAscending = Convert.ToBoolean(dr["IsAscending"]),
                        TimeTaken = Convert.ToInt64(dr["TimeTaken"])
                    }
                );
            }

            return numbers;
        }
    }
}