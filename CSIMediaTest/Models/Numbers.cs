﻿using System;

namespace CSIMediaTest.Models
{
    public class Numbers
    {
        public int Id { get; set; }

        public string NumberEnties { get; set; }

        public bool IsAscending { get; set; }

        public long TimeTaken { get; set; }
    }
}