﻿using CSIMediaTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace CSIMediaTest.Controllers
{
    public class NumbersController : Controller
    {
        // GET: Numbers
        public ActionResult Index()
        {
            var db = new DatabaseAccess();

            ModelState.Clear();

            return View(db.GetNumbers());
        }
        
        // GET: Numbers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Numbers/Create
        [HttpPost]
        public ActionResult Create(Numbers numbersModel)
        {
            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();

                numbersModel.NumberEnties = SortNumbers(numbersModel);

                watch.Stop();

                numbersModel.TimeTaken = watch.ElapsedMilliseconds;
                
                if (ModelState.IsValid)
                {
                    var db = new DatabaseAccess();

                    if (db.AddNumbers(numbersModel))
                    {
                        ViewBag.Message = "Added successfully";
                        ModelState.Clear();
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // Export to XML
        public ActionResult Export()
        {
            var db = new DatabaseAccess();
            var data = db.GetNumbers();
            var Output = new StringWriter(new StringBuilder());
            var xml = new XmlSerializer(data.GetType());
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//Numbers.xml";

            xml.Serialize(Output, data);

            using (StreamWriter file = new StreamWriter(path, true))
            {
                file.Write(Output);
            }

            return RedirectToAction("Index");
        }

        private string SortNumbers(Numbers numbersModel)
        {
            var numbers = numbersModel.NumberEnties.Split(' ').ToList();
            var intNums = numbers.Select(int.Parse).ToList();

            List<int> orderedNums;

            if (numbersModel.IsAscending)
                orderedNums = intNums.OrderBy(i => i).ToList();
            else
                orderedNums = intNums.OrderByDescending(i => i).ToList();

            return string.Join(" ", orderedNums);
        }
    }
}
